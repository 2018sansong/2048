import random as rd


def create_grid(size):
    game_grid = []
    for i in range(0,size):
        game_grid.append([' ' for i in range(size)])
    return game_grid


def grid_add_new_tile(game_grid, line, column):
    game_grid[line][column] = 2
    return game_grid


A = create_grid(4)
grid_add_new_tile(A, 1, 1)
print(A)


def get_all_tiles(game_grid):
    list_tiles = []
    for line in game_grid:
        for element in line:
            if type(element) == int:
                list_tiles.append(element)
            else :
                list_tiles.append(0)
    return list_tiles


def grid_add_new_tile_at_position(game_grid, line, column):
    value = get_value_new_tile()
    game_grid[line][column] = value
    return game_grid


def get_value_new_tile():
    list_values = [2,2,2,2,2,2,2,2,2,4]
    random = rd.randint(0,9)
    return list_values[random]


def get_empty_tiles_positions(game_grid) :
    lines = len(game_grid)
    columns = len(game_grid[0])
    list_empty = []
    for i in range(lines) :
        for j in range(columns) :
            if game_grid[i][j] == 0 or type(game_grid[i][j]) != int:
                list_empty.append((i,j))
    return list_empty


def get_new_position(grid):
    list_empty = get_empty_tiles_positions(grid)
    x,y = rd.choice(list_empty)
    return(x,y)


def grid_get_value(grid, line, column):
    if type(grid[line][column]) == int:
        return grid[line][column]
    else:
        return 0


def init_game(size):
    grid = create_grid(size)
    x1,y1 = get_new_position(grid)
    grid_add_new_tile_at_position(grid, x1, y1)
    x2,y2 = get_new_position(grid)
    grid_add_new_tile_at_position(grid, x2, y2)
    return grid


def grid_to_string(grid,size):
    list_tiles = get_all_tiles(grid)
    grid_str = """"""
    sticks = """ ==="""*size + """ \n"""
    string_grid = sticks
    lines_str = []
    for i in range(size) :
        line = """| """
        numbers = [str(list_tiles[4*i+j]) for j in range(size)]
        line = line + " | ".join(numbers)
        line = line + """|\n"""
        lines_str.append(line)
    grid_str = sticks.join(lines_str)
    grid_str = sticks + grid_str + """ ==="""*4 + """ """
    return grid_str


def long_value(grid) :
    list_tiles = get_all_tiles(grid)
    M = max(list_tiles)
    return len(str(M))


def grid_to_string_with_size(grid,n):
    size = len(grid)
    list_tiles = get_all_tiles(grid)
    grid_str = """"""
    sticks = """ ==="""*size + """ \n"""
    string_grid = sticks
    lines_str = []
    for i in range(size) :
        line = """|"""
        numbers = [str(list_tiles[size*i+j]).ljust(n) for j in range(size)]
        line = line + "|".join(numbers)
        line = line + """|\n"""
        lines_str.append(line)
    grid_str = sticks.join(lines_str)
    grid_str = sticks + grid_str + """ ==="""*size + """ """
    return grid_str


THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


def long_value_with_theme(grid,dic):
    list_tiles = get_all_tiles(grid)
    list_values = []
    for tile in list_tiles:
        if type(dic[tile]) == int:
            list_values.append(len(str(dic[tile])))
        else:
            list_values.append(len(dic[tile]))
    print(list_values)
    max_list = max(list_values)
    print(max_list)
    return max_list


grid = [[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]]
print(long_value(grid))
print(long_value_with_theme(grid,THEMES["0"]))




